Imports System.Xml.Xsl
Imports System.IO

Public Class WebForm1
    Inherits System.Web.UI.Page

    Protected WithEvents Xml1 As System.Web.UI.WebControls.Xml
    Protected WithEvents Xml2 As System.Web.UI.WebControls.Xml

    Const ERR_FILE As String = "EMPTY.xml"
    Const XSL_DETAIL As String = "xslt\Valg-xhtml.xsl"
    Const XSL_FYLKE As String = "xslt\Valg-fylke-xhtml.xsl"
    Const XSL_LISTE_BY As String = "xslt\byliste.xsl"
    Const XML_F01 As String = "xml-docs\F01.xml"
    Const XML_K01 As String = "xml-docs\K01.xml"
    Const XML_F03 As String = "xml-docs\F03.xml"
    Const XML_K03 As String = "xml-docs\K03.xml"
    Private toNotabene As Boolean

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'On Error Resume Next
        'Put user code to initialize the page here

        toNotabene = Request.UserHostAddress = "127.0.0.1" Or Request.UserHostAddress.IndexOf("193.69.125.") > -1

        Dim valgType As String = Request.QueryString("valgType")
        If valgType = "" Then
            valgType = Session("valgType")
            If valgType = "" Then
                valgType = "F"
            End If
        Else
            Session("valgType") = valgType
        End If

        If valgType = "F" Then
            Me.Xml2.DocumentSource = XML_F01
            Me.Xml3.DocumentSource = XML_F03
            Me.Label1.Text = "Fylkestingsvalget"
        Else
            Me.Xml2.DocumentSource = XML_K01
            Me.Xml3.DocumentSource = XML_K03
            Me.Label1.Text = "Kommunevalget"
        End If

        Dim fylkeNr As String = Request.QueryString("FylkeNr")
        Dim liste As String = Request.QueryString("liste")
        Dim kommuneNr As String = Request.QueryString("KommuneNr")
        Dim byNr As String = Request.QueryString("byNr")
        Dim kretsNr As String = Request.QueryString("kretsNr")

        Dim partiKategori As Integer = Session("partiKategori")
        Dim partiKategoriForm As Integer = Request.Form("DropDownList1")

        If partiKategoriForm <> 0 Then
            partiKategori = partiKategoriForm
        End If

        If partiKategori = 0 Then
            partiKategori = 1
        End If

        Session("partiKategori") = partiKategori
        Me.DropDownList1.SelectedIndex = partiKategori - 1

        Dim argList1 As XsltArgumentList = New XsltArgumentList()
        argList1.AddParam("Partikategori", "", partiKategori)
        Me.Xml1.TransformArgumentList = argList1
        'Me.Label2.Text = partiKategori

        Dim navn As String = Request.QueryString("Navn")
        Me.Xml1.TransformSource = XSL_DETAIL

        If byNr <> "" Then
            Dim argList As XsltArgumentList = New XsltArgumentList()
            argList.AddParam("ByNr", "", byNr)
            Me.Xml3.TransformArgumentList = argList
        End If

        'Response.Write(FylkeNr)
        If liste = "07" Then
            Dim xmlFile As String = "xml-docs\" & valgType & liste & ".xml"
            CheckXmlDoc(xmlFile, navn)
            Me.Xml1.TransformSource = XSL_FYLKE
        ElseIf liste <> "" Then
            Dim xmlFile As String = "xml-docs\" & valgType & liste & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf kretsNr <> "" Then
            Dim xmlFile As String = "xml-docs\" & valgType & "03-" & byNr & "-" & kretsNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf kommuneNr <> "" Then
            Dim xmlFile As String = "xml-docs\" & valgType & "02-" & kommuneNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
            If fylkeNr <> "" Then
                Dim argList As XsltArgumentList = New XsltArgumentList()
                argList.AddParam("FylkeNr", "", fylkeNr)
                Me.Xml2.TransformArgumentList = argList
            End If
        ElseIf fylkeNr <> "" Then
            Dim xmlFile As String = "xml-docs\" & valgType & "04-" & fylkeNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
            Dim argList As XsltArgumentList = New XsltArgumentList()
            argList.AddParam("FylkeNr", "", fylkeNr)
            Me.Xml2.TransformArgumentList = argList
        Else
            Dim xmlFile As String = "xml-docs\" & valgType & "05" & ".xml"
            CheckXmlDoc(xmlFile, navn)
        End If

        Me.Button1.Visible = toNotabene
        'Me.Label2.Text = Request.UserHostAddress

    End Sub

    Private Sub CheckXmlDoc(ByVal xmlFile As String, ByVal navn As String)
        Dim testFile As String = Server.MapPath(xmlFile)
        If IO.File.Exists(testFile) Then
            Me.Xml1.DocumentSource = xmlFile
        Else
            Dim argList As XsltArgumentList = New XsltArgumentList()
            argList.AddParam("navn", "", navn)
            Me.Xml1.TransformArgumentList = argList
            Me.Xml1.DocumentSource = ERR_FILE
            Me.Label2.Text = xmlFile
            toNotabene = False
        End If
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error
        Response.Write("Error")
    End Sub

    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Xml3 As System.Web.UI.WebControls.Xml
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
End Class

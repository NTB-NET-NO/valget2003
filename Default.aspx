<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Default.aspx.vb" Inherits="Valget2003.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>NTB - Valg 2003</title>
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<!-- <TABLE id="Table1" style="Z-INDEX: 101; LEFT: 5px; WIDTH: 866px; POSITION: absolute; TOP: 7px; HEIGHT: 502px" cellSpacing="1" cellPadding="1" width="866" border="1"> -->
			<TABLE cellSpacing="0" cellPadding="5" border="1">
				<TR>
					<TD class="NTB" vAlign="top" align="middle" width="200">NTB - Valg 2003</TD>
					<TD width="800">
						<table width="100%">
							<tr>
								<td align="middle"><b><A href="Default.aspx?valgType=F">Fylkestingsvalget</A></b></td>
								<td align="middle"><b><A href="Default.aspx?valgType=F&amp;liste=07">Landsoversikt pr. fylke</A></b></td>
								<td align="middle"><b><A href="Default.aspx?valgType=K">Kommunevalget</A></b></td>
								<td align="middle"><b><A href="Partikoder.aspx" target='_blank"'>Partikoder</A></b></td>
								<td align="right"><asp:dropdownlist id="DropDownList1" runat="server" AutoPostBack="True" Width="150px">
										<asp:ListItem Value="1" Selected="True">Etablerte partier</asp:ListItem>
										<asp:ListItem Value="2">Vis ogs� sm�partier</asp:ListItem>
										<asp:ListItem Value="3">Vis ogs� valglister</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" width="200">
						<h3><asp:label id="Label1" runat="server"></asp:label></h3>
						<h4>Fylkesoversikter:</h4>
						<asp:xml id="Xml2" runat="server" DocumentSource="xml-docs\K01.xml" TransformSource="xslt\fylkesliste.xsl"></asp:xml>
						<h4>Byoversikter:</h4>
						<asp:xml id="Xml3" runat="server" DocumentSource="xml-docs\K03.xml" TransformSource="xslt\byliste.xsl"></asp:xml></TD>
					<TD vAlign="top" width="800">
						<P><asp:xml id="Xml1" runat="server" DocumentSource="xml-docs\F05.xml" TransformSource="xslt\Valg-xhtml.xsl"></asp:xml></P>
						<P><asp:label id="Label2" runat="server"></asp:label></P>
						<P><asp:button id="Button1" runat="server" Text="Send til valgdesken i Notabene" Visible="False"></asp:button></P>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

<xsl:param name="FylkeNr">00</xsl:param>
<xsl:variable name="Form">Default.aspx</xsl:variable>

<xsl:template match="respons">
<table width="100%">
<xsl:apply-templates select="rapport"/>
</table>
</xsl:template>

<!--
<xsl:template match="rapport/liste/data[@navn='FylkeNavn']">

<xsl:template match="rapport/data[@navn='FylkeNavn']">
-->
<xsl:template match="rapport">
<tr><td>
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
		<xsl:text>?FylkeNr=</xsl:text>
		<xsl:if test="data[@navn='FylkeNr']!=$FylkeNr">
			<xsl:value-of select="data[@navn='FylkeNr']"/>
		</xsl:if>			
		<xsl:text>&amp;Navn=</xsl:text>
		<xsl:value-of select="data[@navn='FylkeNavn']"/>
	</xsl:attribute>
	<xsl:value-of select="data[@navn='FylkeNavn']"/>
	</a>
	</td>
	<td class="status" align="right">
	<xsl:value-of select="data[@navn='AntKomm']"/>
	<xsl:text>=</xsl:text>
	<xsl:value-of select="data[@navn='AntKommFhstOpptalt']"/>
	<xsl:text>+</xsl:text>
	<xsl:value-of select="data[@navn='AntKommVtstOpptalt']"/>
	<xsl:text>+</xsl:text>
	<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
</td>
</tr>
<xsl:if test="data[@navn='FylkeNr']=$FylkeNr">
<tr>
	<td colspan="2">
	<table width="100%">
		<xsl:apply-templates select="tabell/liste"/>
	</table>
	</td>
</tr>
</xsl:if>	
</xsl:template>

<xsl:template match="tabell/liste">
<tr>
<td width="20"></td>
<td>
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
		<xsl:text>?FylkeNr=</xsl:text>
		<xsl:value-of select="$FylkeNr"/>
		<xsl:text>&amp;KommuneNr=</xsl:text>
		<xsl:value-of select="data[@navn='KommNr']"/>
		<xsl:text>&amp;Navn=</xsl:text>
		<xsl:value-of select="data[@navn='KommNavn']"/>
	</xsl:attribute>
		<xsl:value-of select="data[@navn='KommNavn']"/>
	</a>
</td>
<td class="status">
		(<xsl:value-of select="data[@navn='StatusInd']"/>)
</td></tr>
	<!--<xsl:apply-templates select="tabell"/>-->
</xsl:template>


</xsl:stylesheet>
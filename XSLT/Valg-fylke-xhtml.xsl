<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="no"/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>

<xsl:param name="navn"></xsl:param>

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:template match="/">
<html>
	<xsl:choose>
	<xsl:when test="$navn=''">
		<xsl:call-template name="rapporthode"/>
		<!-- tabeller i rapporten  -->
		<table class="liste" border="1"  cellspacing="0"  cellpadding="3">
			<xsl:call-template name="tabellhode"/>
			<xsl:apply-templates select="respons/rapport/tabell/liste"/>
			<!--<xsl:call-template name="sum"/>-->
		</table>
	
		<p/>

<!--	
		<table class="status" border="1"  cellspacing="0"  cellpadding="3">
		<xsl:apply-templates select="respons/rapport/data"/>
		</table>
-->
	<table class="statusouter" border="1"  cellspacing="0"  cellpadding="0">
		<tr>
		<td valign="top">
		<table class="status" border="0"  cellspacing="0"  cellpadding="5">
			<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) != 0]"/>
		</table>
		</td>
		<td valign="top">
		<table class="status" border="0"  cellspacing="0"  cellpadding="5">
			<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) = 0]"/>
		</table>
		</td>
		</tr>
	</table>


	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="error"/>
	</xsl:otherwise>
	</xsl:choose>
</html>	
</xsl:template>

<xsl:template name="error">
	<h2>Resultatet for <xsl:value-of select="$navn"/> er ikke klart ennå!</h2>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum">
	<tr>
	<td><i><b>Sum</b></i></td>
	<td align="right"><i><b><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ###', 'no')"/></b></i></td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	</tr>
</xsl:template>

<!-- Template for rader i tabellen -->
<xsl:template match="respons/rapport/tabell[@navn='F07tabell1']/liste">
	<tr>
		<td><xsl:value-of select="data[@navn='FylkeNavn']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='A']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='SV']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='RV']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='SP']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='KRF']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='V']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='H']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='FRP']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='Andre']/data[@navn='ProgProSt']"/></td>

<!--	 <xsl:apply-templates select="tabell[@navn='F07tabell11']/liste/data[@navn='Partikode' and .='A']"/>-->
	</tr>
</xsl:template>

<xsl:template match="respons/rapport/tabell[@navn='F07tabell2']/liste">
</xsl:template>

<xsl:template match="tabell[@navn='F07tabell11']/liste">
	<td><xsl:value-of select="data[@navn='Partikode']"/>: <xsl:value-of select="data[@navn='ProgProSt']"/></td>
</xsl:template>


<!-- Template for kolonner i tabell header-->
<xsl:template name="tabellhode">
	<tr>
		<th>Fylke</th>
		<th>A</th>
		<th>SV</th>
		<th>RV</th>
		<th>SP</th>
		<th>KRF</th>
		<th>V</th>
		<th>H</th>
		<th>FRP</th>
		<th>Andre</th>
	</tr>
</xsl:template>

<xsl:template name="rapporthode">
	<xsl:variable name="rapportnavn">
		<xsl:value-of select="respons/rapport/rapportnavn"/>
	</xsl:variable>
	
	<h1>
	<xsl:value-of select="$rapportnavn"/>
	<xsl:text>: </xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn = 'F01'">
			<xsl:text>Opptalte kommuner - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F02'">
			<xsl:text>Enkeltresultat pr. kommune - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03'">
			<xsl:text>Enkeltoversikt pr. krets - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04'">
			<xsl:text>Fylkesoversikt - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05'">
			<xsl:text>Landsoversikt pr. parti - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F07'">
			<xsl:text>Landsoversikt pr. fylke - fylkestingsvalg</xsl:text>
		</xsl:when>
		
		<xsl:when test="$rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>Enkeltresultat pr. kommune - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K03'">
			<xsl:text>Enkeltoversikt pr. krets - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K05'">
			<xsl:text>Landsoversikt pr. parti - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K07'">
			<xsl:text>Frammøte – 10 høyeste og 10 laveste - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt - kommunevalg</xsl:text>
		</xsl:when>
	</xsl:choose>
	</h1>

	<xsl:choose>
		<xsl:when test="rapport/data[@navn='KommNavn']">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="rapport/data[@navn='FylkeNavn']">
			<h2>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
	</xsl:choose>
	
	<p><b>Siste registrering: <xsl:value-of select="$dato"/>&#160;<xsl:value-of select="rapport/data[@navn='SisteRegTid']"/></b></p>

</xsl:template>

<!-- Template for status-rader i rapporten  -->
<xsl:template match="respons/rapport/data">
	<tr>
	<td><xsl:value-of select="@navn"/>:</td>
	<td align="right">
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:value-of select="."/>
	</td>
	</tr>
</xsl:template>


</xsl:stylesheet>
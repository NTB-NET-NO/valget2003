<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes"/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>

<xsl:param name="navn"></xsl:param>
<xsl:param name="Partikategori">1</xsl:param>

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:template match="respons">
<!--<html>-->
	<xsl:choose>
	<xsl:when test="$navn=''">
		<xsl:call-template name="rapporthode"/>
		<xsl:apply-templates/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="error"/>
	</xsl:otherwise>
	</xsl:choose>
<!--</html>-->
</xsl:template>

<xsl:template name="error">
	<h2>Resultatet for <xsl:value-of select="$navn"/> er ikke klart ennå!</h2>
</xsl:template>

<xsl:template match="Status">
</xsl:template>

<!-- Template for to tabeller i rapporten  -->
<xsl:template match="rapport">
	
	<table class="liste" border="1"  cellspacing="0"  cellpadding="3">
		<tr><xsl:apply-templates select="tabell/liste[1]/data/@navn"/></tr>
		<xsl:apply-templates select="tabell/liste"/>

		<xsl:choose>
		<xsl:when test="$Partikategori='2'">
			<xsl:call-template name="sumandre">
				<xsl:with-param name="kategori" select="3"/>
			</xsl:call-template>
		</xsl:when>
		</xsl:choose>
		
		<xsl:call-template name="sum"/>
	</table>

	<p/>

	<table class="statusouter" border="1"  cellspacing="0"  cellpadding="0">
		<tr>
		<td valign="top">
		<table class="status" border="0"  cellspacing="0"  cellpadding="5">
			<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) != 0]"/>
		</table>
		</td>
		<td valign="top">
		<table class="status" border="0"  cellspacing="0"  cellpadding="5">
			<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) = 0]"/>
		</table>
		</td>
		</tr>
	</table>
	
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum">
	<tr>
	<td><i><b>Sum</b></i></td>
	<td align="right"><i><b><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntFhst']), '### ##0', 'no')"/></b></i></td>
	<td align="right"><i><b><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ##0', 'no')"/></b></i></td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<xsl:if test="/respons/rapport/rapportnavn = 'K02' or /respons/rapport/rapportnavn = 'F02'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	</tr>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sumandre">
	<xsl:param name="kategori"/>
	<tr>
	<td>Andre (<xsl:value-of select="$kategori"/>)</td>
	<td align="right"><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikategori' and data[@navn='Partikategori'] = $kategori]/data[@navn='AntFhst']), '### ##0', 'no')"/></td>
	<td align="right"><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikategori' and data[@navn='Partikategori'] = $kategori]/data[@navn='AntStemmer']), '### ##0', 'no')"/></td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<xsl:if test="/respons/rapport/rapportnavn = 'K02' or /respons/rapport/rapportnavn = 'F02'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	</tr>
</xsl:template>

<!-- Template for status-rader i rapporten  -->
<xsl:template match="/respons/rapport/data">
	<tr>
	<td><xsl:value-of select="@navn"/>:</td>
	<td align="right">
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:value-of select="."/>
	</td>
	</tr>
</xsl:template>


<!-- Template for ubrukte rader i tabellen -->
<xsl:template match="liste">
</xsl:template>

<!-- Template for rader i tabellen -->
<xsl:template match="liste">
	<xsl:if test="data[@navn='Partikategori'] &lt;= $Partikategori and (data[@navn='Partikategori'] != 0 or $Partikategori = 1)">
	<tr><xsl:apply-templates/></tr>
	</xsl:if>
</xsl:template>

<!-- Template for ubrukte kolonner i tabell header-->
<xsl:template match="liste/data/@navn">
</xsl:template>

<!-- Template for kolonner i tabell header-->
<xsl:template match="liste/data[@navn='Partikode' or @navn='AntFhst' or @navn='AntStemmer' or @navn='ProSt' or @navn='DiffPropFFtv' or @navn='DiffPropFKsv' or @navn='DiffPropFStv' or @navn='DiffStFKsv' or @navn='DiffStFStv' or @navn='DiffStFFtv']/@navn">
	<xsl:variable name="navn"><xsl:value-of select="."/></xsl:variable>
	<th>
	<xsl:choose>
		<xsl:when test="$navn='Partikode'">
			<xsl:text>Parti</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntFhst'">
			<xsl:text>Forhåndsstemmer</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntStemmer'">
			<xsl:text>Stemmer</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='ProSt'">
			<xsl:text>And. %</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFFtv'">
			<xsl:text>2003-1999 %</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFKsv'">
			<xsl:text>2003-1999 %</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFStv'">
			<xsl:text>2003-2001 %</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffStFKsv'">
			<xsl:text>Diff. 1999</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffStFStv'">
			<xsl:text>Diff. 2001</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffStFFtv'">
			<xsl:text>Diff. 1999</xsl:text>
		</xsl:when>
		
		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
	</th>
</xsl:template>

<!-- Template for ubrukte kolonner i tabellen -->
<xsl:template match="liste/data">
</xsl:template>

<!-- Template for kolonner i tabellen -->
<xsl:template match="liste/data[@navn='Partikode' or @navn='AntFhst' or @navn='AntStemmer' or @navn='ProSt' or @navn='DiffPropFFtv' or @navn='DiffPropFKsv' or @navn='DiffPropFStv' or @navn='DiffStFKsv' or @navn='DiffStFStv' or @navn='DiffStFFtv']">
	<td>
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:choose>
		<xsl:when test="position() = 3">
			<xsl:attribute name="align">right</xsl:attribute>
			<xsl:value-of select="format-number(., '### ##0', 'no')"/>
		</xsl:when>
		<xsl:when test="position() &gt; 1">
			<xsl:attribute name="align">right</xsl:attribute>
			<xsl:value-of select="."/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
	
	<xsl:if test=".=''">&#160;</xsl:if>
	</td>
</xsl:template>

<xsl:template name="rapporthode">
	<xsl:variable name="rapportnavn">
		<xsl:value-of select="rapport/rapportnavn"/>
	</xsl:variable>
	
	<h1>
	<xsl:value-of select="$rapportnavn"/>
	<xsl:text>: </xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn = 'F01'">
			<xsl:text>Opptalte kommuner - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F02'">
			<xsl:text>Enkeltresultat pr. kommune - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03'">
			<xsl:text>Enkeltoversikt pr. krets - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04'">
			<xsl:text>Fylkesoversikt - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05'">
			<xsl:text>Landsoversikt pr. parti - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F07'">
			<xsl:text>Landsoversikt pr. fylke - fylkestingsvalg</xsl:text>
		</xsl:when>
		
		<xsl:when test="$rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>Enkeltresultat pr. kommune - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K03'">
			<xsl:text>Enkeltoversikt pr. krets - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K05'">
			<xsl:text>Landsoversikt pr. parti - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K07'">
			<xsl:text>Frammøte – 10 høyeste og 10 laveste - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt - kommunevalg</xsl:text>
		</xsl:when>
	</xsl:choose>
	</h1>

	<!-- Skriv Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KretsNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			</h2>
		</xsl:when>
	</xsl:choose>
	
	<p class="regdato"><b>Siste registrering: <xsl:value-of select="$dato"/>&#160;<xsl:value-of select="rapport/data[@navn='SisteRegTid']"/></b> (Status: <xsl:value-of select="Status"/>)</p>

</xsl:template>

</xsl:stylesheet>
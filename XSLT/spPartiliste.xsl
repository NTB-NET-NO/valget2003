<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="/">
<TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">

<xsl:call-template name="tablehead"/>
<xsl:apply-templates/>

</TABLE>
</xsl:template>

<xsl:template match="dataroot/spPartiliste">
	<tr>
	<td><xsl:value-of select="forkortning"/></td>
	<td><xsl:value-of select="navn"/></td>
	<td align="center">&#160;<xsl:if test="offisiellt='1'">*</xsl:if></td>
	<td align="center">&#160;<xsl:if test="fellesliste='1'">*</xsl:if></td>
	</tr>
</xsl:template>

<xsl:template name="tablehead">
	<TR>
	<TH style="width: 2cm">
	<xsl:text>Partikode</xsl:text>
	</TH>
	<TH style="width: 8cm">
	<xsl:text>Partinavn</xsl:text>
	</TH>
	<TH style="width: 1cm">
	<xsl:text>Offisiellt</xsl:text>
	</TH>
	<TH style="width: 1cm">
	<xsl:text>Fellesliste</xsl:text>
	</TH>
	</TR>
</xsl:template>

<xsl:template match="spPartiliste">

</xsl:template>

</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

<xsl:param name="ByNr"></xsl:param>
<xsl:variable name="Form">Default.aspx</xsl:variable>

<xsl:template match="respons">
<table width="100%">
	<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='0301'][1]" mode="by"/>
	<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='1201'][1]" mode="by"/>
	<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='1103'][1]" mode="by"/>
	<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='1601'][1]" mode="by"/>

<!--
	<xsl:call-template name="byer">
		<xsl:with-param name="by">Oslo</xsl:with-param>
		<xsl:with-param name="KommNr">0301</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="byer">
		<xsl:with-param name="by">Bergen</xsl:with-param>
		<xsl:with-param name="KommNr">1201</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="byer">
		<xsl:with-param name="by">Stavanger</xsl:with-param>
		<xsl:with-param name="KommNr">1103</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="byer">
		<xsl:with-param name="by">Trondheim</xsl:with-param>
		<xsl:with-param name="KommNr">1601</xsl:with-param>
	</xsl:call-template>
-->
	
</table>
</xsl:template>

<xsl:template match="/respons/rapport" mode="by">
	<xsl:variable name="KommNr"><xsl:value-of select="data[@navn='KommNr']"/></xsl:variable>
	<xsl:variable name="KommNavn"><xsl:value-of select="data[@navn='KommNavn']"/></xsl:variable>
	<tr><td>
		<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?ByNr=</xsl:text>
			<xsl:if test="$KommNr != $ByNr">
				<xsl:value-of select="$KommNr"/>
			</xsl:if>
			<xsl:text>&amp;KommuneNr=</xsl:text>
			<xsl:value-of select="$KommNr"/>

			<xsl:text>&amp;Navn=</xsl:text>
			<xsl:value-of select="$KommNavn"/>
		</xsl:attribute>
		<xsl:value-of select="$KommNavn"/>
		</a>
	<td class="status" align="right">
		<xsl:value-of select="data[@navn='TotAntKretser']"/>
		<xsl:text>=</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserFhstOpptalt']"/>
		<xsl:text>+</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserVtstOpptalt']"/>
		<xsl:text>+</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserAltOpptalt']"/>
	</td>
	</td></tr>
	<xsl:if test="$KommNr=$ByNr">
	<tr><td colspan="2">
	<table>
		<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr'] = $KommNr and data[@navn='AntFrammotte'] != 0]" mode="krets"/>
	</table>
	</td></tr>
	</xsl:if>
</xsl:template>

<!--
<xsl:template name="byer">
	<xsl:param name="by"/>
	<xsl:param name="KommNr"/>
	<tr><td>
		<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?ByNr=</xsl:text>
			<xsl:if test="$KommNr!=$ByNr">
				<xsl:value-of select="$KommNr"/>
			</xsl:if>			
			<xsl:text>&amp;KommuneNr=</xsl:text>
			<xsl:value-of select="$KommNr"/>

			<xsl:text>&amp;Navn=</xsl:text>
			<xsl:value-of select="$by"/>
		</xsl:attribute>
		<xsl:value-of select="$by"/>
		</a>
	<td>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="data[@navn='TotAntKretser']"/>
		<xsl:text>=</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserFhstOpptalt']"/>
		<xsl:text>+</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserVtstOpptalt']"/>
		<xsl:text>+</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserAltOpptalt']"/>
		<xsl:text>)</xsl:text>
	</td>
	</td></tr>
	<xsl:if test="$KommNr=$ByNr">
	<tr><td>
	<table>
		<xsl:apply-templates select="rapport[data[@navn='KommNr'] = $KommNr and data[@navn='AntFrammotte'] != '']"/>
	</table>
	</td></tr>		
	</xsl:if>
</xsl:template>
-->

<xsl:template match="*">
</xsl:template>

<xsl:template match="/respons/rapport" mode="krets">
<tr>
<td width="20"></td>
<td>
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
		<xsl:text>?ByNr=</xsl:text>
		<xsl:value-of select="$ByNr"/>
		<xsl:text>&amp;KretsNr=</xsl:text>
		<xsl:value-of select="data[@navn='KretsNr']"/>
		<xsl:text>&amp;Navn=</xsl:text>
		<xsl:value-of select="data[@navn='KretsNavn']"/>
	</xsl:attribute>
		<xsl:value-of select="data[@navn='KretsNavn']"/>
	</a>
</td>	
</tr>	
</xsl:template>


</xsl:stylesheet>